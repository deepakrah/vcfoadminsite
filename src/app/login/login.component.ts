import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from 'app/theme/layout/admin/admin.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public LoginId;
  public Password;
  public User = [];
  LoggedInUserId: number;
  constructor(
    private _AdminService: AdminService,
    // _LocalStorage: LocalStorageService,
    private _ToastrService: ToastrService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
  }
  Login() {
    this.spinner.show();
    debugger
    //if (this.LoginId == "admin" && this.Password == "123456") {
    let UserObj = {
      LoginID: this.LoginId,
      Password: this.Password
    }
    //this._LocalStorage.storeOnLocalStorage("Selected", "0");
    //this._LocalStorage.storeOnLocalStorage("LoggedInUserId", "1");
    //this.spinner.hide();
    //this.router.navigate(['/home']);
    this._AdminService.ValidateUser(UserObj).subscribe(_user => {
      this.spinner.hide();
      this.User = _user;
      if (_user != null) {
        if (this.User.length > 0) {
          //console.log(this.User)

          localStorage.setItem("LoggedInUserId", this.User[0].userId.toString());
          localStorage.setItem("LoggedInUserType", this.User[0].userType.toString());
          localStorage.setItem("Name", this.User[0].name.toString());
          localStorage.setItem("Token", this.User[0].token);
          // this._LocalStorage.storeOnLocalStorage("Selected", "0");
          //this._LocalStorage.storeOnLocalStorage("LoggedInUserType","2");
          // if (this.User[0].branchId.toString() != "" || this.User[0].branchId != null) {
          this.router.navigate(['/admin']);
          // }
          // else {
          //   //$('#spinner').css('display', 'none');
          //   this._ToastrService.error("Something went wrong.");

          // }
        }
        else {
          //$('#spinner').css('display', 'none');
          this._ToastrService.error("Invalid username and password");
        }
      }
      else {
        //$('#spinner').css('display', 'none');
        this._ToastrService.error("Invalid username and password", 'Authentication failed');
      }
    },
      err => {
        if (err.status == 400) {
          this._ToastrService.error("Invalid username and password");
        }
      }
    )
    // }
    // else {
    //   this.spinner.hide();
    //   this._ToastrService.error("Invalid username and password");
    //   return;
    // }
  }
}

