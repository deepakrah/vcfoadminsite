import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  private BASE_API_URL = environment.apiUrl;
  private _controllerName: string = "Admin/";
  private _url: string = this.BASE_API_URL + this._controllerName;
  private _methodName: string = "";
  private _param: {};
  private httpOptions = {
    _headers: new HttpHeaders({
      'Content-Type': 'applicantion/json'
    })
  };
  constructor(private _http: HttpClient) { }

  ValidateUser(_obj: any): Observable<any> {
    this._methodName = "ValidateUser/";
    this._param = _obj;
    return this._http.post<any>(
      this._url + this._methodName, this._param
    );
  }

  GetAllUsers(): Observable<any> {
    this._methodName = "GetAllUsers/";
    this._param = {};
    return this._http.post<any>(
      this._url + this._methodName, this._param
    );
  }

  GetAllOrders(obj: any): Observable<any> {
    this._methodName = "GetAllOrders/";
    this._param = obj;
    return this._http.post<any>(
      this._url + this._methodName, this._param
    );
  }

  DocPath(_obj: any): Observable<any[]> {
    this._methodName = "DocPath/";
    this._param = _obj;
    return this._http.post<any[]>(
      this._url + this._methodName, this._param
    );
  }
}
