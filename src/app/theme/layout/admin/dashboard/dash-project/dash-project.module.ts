import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashProjectRoutingModule } from './dash-project-routing.module';
import { DashProjectComponent } from './dash-project.component';
 
import {NgbProgressbarModule} from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from '@shared/shared.module';


@NgModule({
  declarations: [DashProjectComponent],
  imports: [
    CommonModule,
    DashProjectRoutingModule,
    SharedModule,
    NgbProgressbarModule
  ]
})
export class DashProjectModule { }
