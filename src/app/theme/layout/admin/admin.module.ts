import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import {DataTablesModule} from 'angular-datatables';
import { ToastrModule } from 'ngx-toastr';
const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashbaord'
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./dashboard/dashboard.module').then(module => module.DashboardModule)
  },
  {
    path: 'masters',
    loadChildren: () => import('./masters/masters.module').then(module => module.MastersModule)
  }
    
]


@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes),
    FormsModule,
    DataTablesModule,
    ToastrModule
  ],
  providers: [
    DatePipe
  ],
  exports: [RouterModule]
})
export class AdminModule { }
