import {Injectable} from '@angular/core';

export interface NavigationItem {
  id: string;
  title: string;
  type: 'item' | 'collapse' | 'group';
  translate?: string;
  icon?: string;
  hidden?: boolean;
  url?: string;
  classes?: string;
  exactMatch?: boolean;
  external?: boolean;
  target?: boolean;
  breadcrumbs?: boolean;
  function?: any;
  badge?: {
    title?: string;
    type?: string;
  };
  children?: Navigation[];
}

export interface Navigation extends NavigationItem {
  children?: NavigationItem[];
}

const NavigationItems = [
  {
    id: 'navigation',
    title: 'Navigation',
    type: 'group',
    icon: 'feather icon-align-left',
    children: [
      {
        id: 'Dashboard',
        title: 'Dashboard',
        type: 'item',
        url: 'admin/dashboard/analytics',
        classes: 'nav-item',
        icon: 'feather icon-sidebar'
      },
      {
        id: 'menu-level',
        title: 'Masters',
        type: 'collapse',
        icon: 'feather icon-menu',
        children: [
          {
            id: 'category',
            title: 'Category',
            type: 'item',
            url: 'admin/masters/category',
            
          },
          {
            id: 'services',
            title: 'Services',
            type: 'item',
            url: 'admin/masters/services',
            
          },
          {
            id: 'User',
            title: 'User',
            type: 'item',
            url: 'admin/masters/User',
          },
          {
            id: 'Orders',
            title: 'Orders',
            type: 'item',
            url: 'admin/masters/Orders',
          },
          {
            id: 'ContactUs',
            title: 'Contact us',
            type: 'item',
            url: 'admin/masters/ContactUs',
          },
          // {
          //   id: 'items',
          //   title: 'Items',
          //   type: 'item',
          //   url: 'admin/masters/items',
            
          // },
          // {
          //   id: 'menu-level-2.2',
          //   title: 'Locations',
          //   type: 'item',
          //   url: 'admin/masters/locations',
            
          // },
          // {
          //   id: 'menu-level-2.3',
          //   title: 'Units',
          //   type: 'item',
          //   url: 'admin/masters/units',
            
          // },
          // {
          //   id: 'hsn',
          //   title: 'HSN/SAC',
          //   type: 'item',
          //   url: 'admin/masters/hsn',
          // },
          //  {
          //   id: 'users',
          //   title: 'Users & Roles',
          //   type: 'item',
          //   url: 'admin/masters/usersroles',
          // },
          // {
          //   id: 'invoice',
          //   title: 'Invoice',
          //   type: 'item',
          //   url: 'admin/masters/invoice',
          // }
        ]
      }
    ]
  }
];

@Injectable()
export class NavigationItem {
  public get() {
    return NavigationItems;
  }
}
