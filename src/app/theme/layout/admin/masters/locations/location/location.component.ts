import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { LocationService } from '../location.service';
import { CompanyService } from '../../companies/company.service';
@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss']
})
export class LocationComponent implements OnInit, OnDestroy {
  form: FormGroup;
  companies: any[];
  subscriptions: Subscription[] = [];
  isSubmit = false;
  updateId;
  updateObjectValues;
  constructor(private _formBuilder: FormBuilder, private locationService: LocationService,
    private router: Router, private route: ActivatedRoute,
    private companyService: CompanyService) {

    this.subscriptions.push(this.route.params.subscribe(params => {
      this.updateId = params.id;
    }));
    if (this.router.getCurrentNavigation() && this.router.getCurrentNavigation().extras && this.router.getCurrentNavigation().extras.state) {
      this.updateObjectValues = this.router.getCurrentNavigation().extras.state;
    }


  }


  ngOnInit(): void {
    this.loadCompanies();
    // Reactive Form
    this.form = this._formBuilder.group({
      name: ['', Validators.required],
      address1: [''],
      address2: [''],
      city: [''],
      state: [''],
      pincode: ['',],
      phone: ['', Validators.required],
      email: ['', Validators.required],
      companyId: [0, Validators.required],
      isPrimary: new FormControl(false)
    });

    this.patchEditFormGroupValues();
  }

  patchEditFormGroupValues() {
    if (this.updateObjectValues) {
      const objectValueJSON = JSON.parse(this.updateObjectValues);
      for (const frmCtrl in this.form.controls) {
        if(frmCtrl !== 'isPrimary'){
          this.form.controls[frmCtrl].setValue(objectValueJSON[frmCtrl]);
        }
        else{
          this.form.controls[frmCtrl].setValue(objectValueJSON[frmCtrl] == "Yes" ? true: false);
        }
      }
    }
  }

  get f() {
    return this.form.controls;
  }
  loadCompanies() {
    this.subscriptions.push(this.companyService.getAll().subscribe((results: any[]) => {
      this.companies = results;
      if(this.updateId){
        this.patchEditFormGroupValues();
      }
    },
      error => console.error(error)
    ))
  }
  save() {
    this.isSubmit = true;
    if (this.form.valid) {
      if(this.updateId)
      {
        this.form.value['id'] = parseInt(this.updateId);
      }
      this.form.value['isPrimary'] = this.form.value['isPrimary'] ? 1 : 0;

      var d: any = {};
      Object.assign(d, this.form.value);
      d.companyId = parseInt(d.companyId);

      this.subscriptions.push(this.locationService.save(d, this.updateId).subscribe(result => {
        this.form.reset();
        if(this.updateId)
        {
          this.router.navigate(["/admin/masters/locations"]);
        }
      },
        error => {
          console.error(error);

        }
      ));
    }
  }
  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe())
  }

}
