import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { LocationService } from '../location.service';

@Component({
  selector: 'app-locations-list',
  templateUrl: './locations-list.component.html',
  styleUrls: ['./locations-list.component.scss']
})
export class LocationsListComponent implements OnInit, OnDestroy {
  rows: any[];
  loadingIndicator: boolean;
  reorderable: boolean;
  subscriptions: Subscription[] = []
  constructor(private locationService: LocationService) { }

  ngOnInit(): void {
    this.subscriptions.push(this.locationService.getAll().subscribe((d: any) => {
      this.rows = d.map((item)=> { item.isPrimary = item.isPrimary === 1 ? 'Yes':'No'; return item});
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe())
  }

  getJsonStr(json){
    return JSON.stringify(json);
  }

}
