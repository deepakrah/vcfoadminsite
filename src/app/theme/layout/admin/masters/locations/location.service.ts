import { Injectable } from '@angular/core';
import {DataService} from '@shared/services/data.service';

@Injectable({
  providedIn: 'root'
})
export class LocationService {
  baseUrl = 'location';
  constructor(private dataService: DataService) { }


  getAll() {
    return this.dataService.get(this.baseUrl + '/all');
  }
  save(json_data,id) {
    if(id){
      return this.dataService.post(this.baseUrl + '/update', json_data);
    }else{
      return this.dataService.post(this.baseUrl + '/add', json_data);
    }
    
  }
}
