import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { MastersSharedService } from "../../masters-shared.service";
import { HSNService } from '../hsn.service';

@Component({
  selector: 'app-hsn',
  templateUrl: './hsn.component.html',
  styleUrls: ['./hsn.component.scss']
})
export class HSNComponent implements OnInit, OnDestroy {
  form: FormGroup;
  units: any[];
  subscriptions: Subscription[] = [];
  updateId;
  updateObjectValues;
  constructor(private _formBuilder: FormBuilder, 
    private router: Router, private route: ActivatedRoute,
    private mastersSharedService: MastersSharedService, private hsnService: HSNService) {
      this.subscriptions.push(this.route.params.subscribe( params => {
        this.updateId = params.id;
      }));
      if(this.router.getCurrentNavigation() && this.router.getCurrentNavigation().extras && this.router.getCurrentNavigation().extras.state){
        this.updateObjectValues = this.router.getCurrentNavigation().extras.state;
      }
    }


  ngOnInit(): void {

    // Reactive Form
    this.form = this._formBuilder.group({
      name: ['', Validators.required],
      code: ['', Validators.required]
    });
    this.patchEditFormGroupValues();
  }

  patchEditFormGroupValues(){
    if(this.updateObjectValues){
      const objectValueJSON = JSON.parse(this.updateObjectValues);
      for(const frmCtrl in this.form.controls){
        this.form.controls[frmCtrl].setValue(objectValueJSON[frmCtrl]);
      }
    }
  }

  save() {
    if (this.form.valid) {
      if(this.updateId)
      {
        this.form.value['id'] = parseInt(this.updateId);
      }
      const json_str = JSON.stringify(this.form.value);
      this.subscriptions.push(this.hsnService.save(json_str, this.updateId).subscribe(result => {
        this.form.reset();
        if(this.updateId)
        {
          this.router.navigate(["/admin/masters/hsn"]);
        }
       
      },
        error => {
          console.error(error);
        }
      ));
    }
  }
  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe())
  }
}
