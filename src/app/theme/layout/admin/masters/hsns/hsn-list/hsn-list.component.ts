import { Component, OnDestroy, OnInit } from '@angular/core';
import {HSNService} from '../hsn.service';
import { Subscription } from 'rxjs';
import { Router,ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-hsn-list',
  templateUrl: './hsn-list.component.html',
  styleUrls: ['./hsn-list.component.scss']
})
export class HSNListComponent implements OnInit,OnDestroy {

  rows: any[];
  loadingIndicator: boolean;
  reorderable: boolean;
  subscriptions: Subscription[] = [];
  dtColumnsReorderOptions: any = {};

  constructor(private hsnService: HSNService,private router: Router,private route: ActivatedRoute) { 
  }
  
  ngOnInit(): void {
    this.subscriptions.push(this.hsnService.getAll().subscribe((d: any) => {
      this.rows = d;
    }));
     
  }
  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe())
  }

  getJsonStr(json){
    return JSON.stringify(json);
  }

}
