import { Component, Input, OnInit, forwardRef, Output, EventEmitter } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ControlValueAccessor, FormControl, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ItemService } from '../items/item.service';

// const OPTIONS = [
//   {label: 'ABC', value: 0},
//   {label: 'ABD', value: 1},
//   {label: 'BCD', value: 2},
//   {label: 'BDe', value: 3},
//   {label: 'E', value: 4}
// ];


@Component({
  selector: 'app-autocomplete3',
  templateUrl: './autocomplete3.component.html',
  styleUrls: ['./autocomplete3.component.scss'],
  
	providers: [{
		provide: NG_VALUE_ACCESSOR,
		useExisting: forwardRef(() => Autocomplete3Component),
		multi: true
	}]
})
export class Autocomplete3Component implements OnInit {
  @Output() selectedItemEvent = new EventEmitter<string>();

  @Input()
  parentForm: FormGroup;
  
  filter$ = new BehaviorSubject('');
  control = new FormControl('');
  
  itemList = [];

  @Input() data=[];
  @Input() required = false;
  private _value: {label: string, value: number};
  @Input() set value(value: {label: string, value: number}) {
    this._value = value;
    this.propagateChange(value);
  }
  get value() { return this._value; }
  listItem=[]
	propagateChange: (val: any) => void = () => {};
	propagateTouched: () => void = () => {};
  @Input() keyword = 'name';
 constructor(  private itemService: ItemService){}
  ngOnInit() {
    // this.control.valueChanges.subscribe((value) => {
    //   this.propagateChange(null);
    //   this.filter$.next(value);
    // }, () => {}, () => this.filter$.complete());
    // this.setupOptions();

    this.itemService.getAll().subscribe((data : any)=>{
      this.listItem= data;
    });
  }

  setupOptions() {
    // this.filter$.subscribe(filter => {
    //   this.options$.next(OPTIONS.filter(option => option.label.startsWith(filter)));
    // });
  }

  getLabel(value: {label: string, value: number}) {
    return value && value.label || '';
  }

	writeValue(value: any) {
		if (this.value !== value) {
			this.value = value;
		}
	}
	registerOnChange(fn: (val: any) => void) {
		this.propagateChange = fn;
	}
	registerOnTouched(fn: () => void) {
		this.propagateTouched = fn;
	}


 

  selectItemEvent(item: any) {
    // do something with selected item
   //alert(JSON.stringify(item));

   this.selectedItemEvent.emit(item.Id);
    // (<FormArray>this.form.controls['units']).at(index).patchValue({
    //   unitName: item
    // });
    // //   
    // this.form.updateValueAndValidity()
    // this.addUnit()
    // console.log(this.form.value);
  }

  onItemChangeSearch(name: string) {
    //alert(name);
    // return this.data.filter((n: any)=> n.name.toLowerCase()== name.toLowerCase())
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
    // if (name && name.length >= 0) {
    //   this.itemService.getItembyname({ name: name })
    //     .subscribe((res: any) => {
    //       if (res == undefined) {
    //         this.itemList = [];
    //       }
    //       if (res && res.length > 0) {
    //         this.itemList = res.map((opt: any) => {
    //           return opt.name ;
    //         });
    //       }

    //     },
    //       err => {
    //         //  alert(JSON.stringify(err));
    //       });
    // }
  }

  onItemFocused(e) {
       // do something when input is focused
  }

}
