import { Injectable } from '@angular/core';
import { environment } from '../../../../../../environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContactusService {
  private BASE_API_URL = environment.apiUrl;
  private _controllerName: string = "ContactUs/";
  private _url: string = this.BASE_API_URL + this._controllerName;
  private _methodName: string = "";
  private _param: {};
  private httpOptions = {
    _headers: new HttpHeaders({
      'Content-Type': 'applicantion/json'
    })
  };
  constructor(private _http: HttpClient) { }

  GetContactUs(): Observable<any> {
    this._methodName = "GetContactUs/";
    this._param = {};
    return this._http.post<any>(
      this._url + this._methodName, this._param
    );
  }
}
