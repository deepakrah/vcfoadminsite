import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ContactusService } from '../contact-us/contactus.service';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {
  rows: any = [];
  constructor(
    private spinner: NgxSpinnerService,
    private _toasterService: ToastrService,
    private _ContactusService: ContactusService
  ) { }

  ngOnInit(): void {
    this.spinner.show();
    this._ContactusService.GetContactUs()
      .subscribe(res => {
        this.rows = res;
        this.spinner.hide();
      });
  }

}
