// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-autocomplete',
//   template: `
//     <p>
//       autocomplete works!
//     </p>
//   `,
//   styles: [
//   ]
// })
// export class AutocompleteComponent implements OnInit {

//   constructor() { }

//   ngOnInit(): void {
//   }

// }
import { Component, Input, OnInit, forwardRef } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ItemService } from '../items/item.service';

const OPTIONS = [
  {label: 'ABC', value: 0},
  {label: 'ABD', value: 1},
  {label: 'BCD', value: 2},
  {label: 'BDe', value: 3},
  {label: 'E', value: 4}
];
const OPTIONS2 = [
  {label: 'horse', value: 100},
  {label: 'cat', value: 200},
  {label: 'dinosaur', value: 300},
  {label: 'dog', value: 400},
  {label: 'zebra', value: 500},
  {label: 'penguin', value: 600}
];

// export const MAT_AUTOCOMPLETE_TEMPLATE = `
// <mat-input-container>
// 	<input
// 		matInput placeholder="placeholder"
// 		name="search" [formControl]="control" [required]="required"
// 		[matAutocomplete]="autoSearch"
//     [ngModel]="value"
// 	>
//   <mat-error *ngIf="control.errors?.required">Required</mat-error>
// </mat-input-container>
// <mat-autocomplete #autoSearch="matAutocomplete" [displayWith]="getLabel" (optionSelected)="value = $event.option.value">
// 	<mat-option *ngFor="let option of options$ | async" [value]="option" fxLayout="row wrap" fxLayoutWrap="wrap" fxLayoutAlign="stretch center">
// 		{{option.label}}
// 	</mat-option>
// </mat-autocomplete>
// `;
export const HtmlTEMPLATE = `
<br> <div class="ng-autocomplete"  >
<ng-autocomplete [data]="data" [searchKeyword]="keyword"
    placeHolder="Search item" 
    (selected)='selectItemEvent($event)'
    (inputChanged)='onItemChangeSearch($event)'
    (inputFocused)='onItemFocused($event)' [itemTemplate]="itemTemplate"
    [notFoundTemplate]="notFoundItemTemplate" minQueryLength="3"
    [debounceTime]="100"    >
</ng-autocomplete>
<ng-template #itemTemplate let-item>
<a [innerHTML]="item.name"></a>
</ng-template>
<ng-template #notFoundItemTemplate let-notFound>
    <div [innerHTML]="notFound"></div>
</ng-template>
</div>
`;

export const MAT_AUTOCOMPLETE_STYLES = [``];

@Component({
  selector: 'app-autocomplete',
  template: HtmlTEMPLATE,
  styles: MAT_AUTOCOMPLETE_STYLES,
	providers: [{
		provide: NG_VALUE_ACCESSOR,
		useExisting: forwardRef(() => AutocompleteComponent),
		multi: true
	}]
})
export class AutocompleteComponent implements OnInit, ControlValueAccessor {
  options$ = new BehaviorSubject([]);
  filter$ = new BehaviorSubject('');
  control = new FormControl('');
  @Input()

  @Input() data=[];
  @Input() required = false;
  private _value: {label: string, value: number};
  @Input() set value(value: {label: string, value: number}) {
    this._value = value;
    this.propagateChange(value);
  }
  get value() { return this._value; }
  listItem=[]
	propagateChange: (val: any) => void = () => {};
	propagateTouched: () => void = () => {};
  @Input() keyword = 'name';
 constructor(  private itemService: ItemService){}
  ngOnInit() {
    this.control.valueChanges.subscribe((value) => {
      this.propagateChange(null);
      this.filter$.next(value);
    }, () => {}, () => this.filter$.complete());
    this.setupOptions();
    this.itemService.getAll().subscribe((data : any)=>{
      this.listItem= data;
    });
  }

  setupOptions() {
    this.filter$.subscribe(filter => {
      this.options$.next(OPTIONS.filter(option => option.label.startsWith(filter)));
    });
  }

  getLabel(value: {label: string, value: number}) {
    return value && value.label || '';
  }

	writeValue(value: any) {
		if (this.value !== value) {
			this.value = value;
		}
	}
	registerOnChange(fn: (val: any) => void) {
		this.propagateChange = fn;
	}
	registerOnTouched(fn: () => void) {
		this.propagateTouched = fn;
	}


 
  itemList = [];

  selectItemEvent(item: any) {
    // do something with selected item
    // alert(JSON.stringify(item));
    // (<FormArray>this.form.controls['units']).at(index).patchValue({
    //   unitName: item
    // });
    // //   
    // this.form.updateValueAndValidity()
    // this.addUnit()
    // console.log(this.form.value);
  }

  onItemChangeSearch(name: string) {
    //alert(name);
    // return this.data.filter((n: any)=> n.name.toLowerCase()== name.toLowerCase())
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
    // if (name && name.length >= 0) {
    //   this.itemService.getItembyname({ name: name })
    //     .subscribe((res: any) => {
    //       if (res == undefined) {
    //         this.itemList = [];
    //       }
    //       if (res && res.length > 0) {
    //         this.itemList = res.map((opt: any) => {
    //           return opt.name ;
    //         });
    //       }

    //     },
    //       err => {
    //         //  alert(JSON.stringify(err));
    //       });
    // }
  }

  onItemFocused(e) {
       // do something when input is focused
  }
}


