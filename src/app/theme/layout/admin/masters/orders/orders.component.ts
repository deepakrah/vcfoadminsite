import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { AdminService } from '../../admin.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { environment } from '@environment/environment';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
  rows: any[];
  StartDate: any = this._datePipe.transform(new Date().toString(), 'yyyy-MM-dd');
  EndDate: any = this._datePipe.transform(new Date().toString(), 'yyyy-MM-dd');
  openedModalRef;
  @ViewChild('viewDocModal') public viewDocModal;
  lstViewDoc: any;
  SelectedOrderId: any;
  SelectedUserId: any;
  DownloadPath = environment.DownloadPath;
  constructor(
    modelConfig: NgbModalConfig,
    private modalService: NgbModal,
    private _adminService: AdminService,
    private _datePipe: DatePipe,
    private spinner: NgxSpinnerService
  ) {
    modelConfig.centered = true;
    modelConfig.backdrop = 'static';
    modelConfig.keyboard = false;
    modelConfig.size = 'md';
  }

  ngOnInit(): void {
    this.Search();
  }

  Search() {
    this.spinner.show();
    let obj = {
      StartDate: this._datePipe.transform(new Date(this.StartDate).toString(), 'yyyy-MM-dd'),
      EndDate: this._datePipe.transform(new Date(this.EndDate).toString(), 'yyyy-MM-dd')
    }
    this._adminService.GetAllOrders(obj).subscribe(res => {
      this.rows = res;
      this.spinner.hide();
    });
  }

  openModalViewDoc(modalRef, item) {
    debugger
    this.SelectedOrderId = Number(item.id);
    this.SelectedUserId = Number(item.userId);
    let obj = {
      userId: Number(item.userId),
      OrderId: Number(item.id)
    };
    this.spinner.show();
    this._adminService.DocPath(obj).subscribe(res => {
      debugger
      this.lstViewDoc = res;
      this.openedModalRef = this.modalService.open(modalRef);
      this.spinner.hide();
    });


  }

  closeModal() {
    this.openedModalRef.close();
  }
}
