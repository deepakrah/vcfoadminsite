import { Injectable } from '@angular/core';
import {DataService} from '@shared/services/data.service';

@Injectable({
  providedIn: 'root'
})
export class MastersSharedService {

  itemUnitUrl='itemunit';
  constructor(private dataService: DataService) { }
  
  getAllItemUnit(){
    return this.dataService.get(this.itemUnitUrl + '/all');
  }
}
