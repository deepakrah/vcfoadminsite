import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { MastersSharedService } from "../../masters-shared.service";
import { ItemService } from '../item.service';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit, OnDestroy {
  form: FormGroup;
  units: any[];
  subscriptions: Subscription[] = [];
  constructor(private _formBuilder: FormBuilder, private mastersSharedService: MastersSharedService, private itemService: ItemService) { }


  ngOnInit(): void {
    this.loadUnits();
    // Reactive Form
    this.form = this._formBuilder.group({
      typeId: [0, Validators.required],
      name: ['', Validators.required],
      code: ['', Validators.required],
      description: [''],
      unitid: [0, Validators.required],
    });


  }

  get f() {
    return this.form.controls;
  }
  loadUnits() {
    this.subscriptions.push(this.mastersSharedService.getAllItemUnit().subscribe((results: any[]) => {
      this.units = results;
    },
      error => console.error(error)
    ));
  }
  isSubmit = false;
  add() {
    this.isSubmit = true;
    if (this.form.valid) {
      var d :any = {};
      Object.assign(d, this.form.value);
      d.unitid = parseInt(d.unitid);
      d.typeId = parseInt(d.typeId);
      this.subscriptions.push(this.itemService.add(d).subscribe(result => {
        this.form.reset();
        // this._snackBar.open('Item added', 'Close', {
        //   duration: 2000,
        // });
      },
        error => {
          console.error(error);
          // this._snackBar.open('Failure in Adding Item', 'Close', {
          //   duration: 2000,
          // });
        }
      ));
    }
  }
  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe())
  }
}
