import { Injectable } from '@angular/core';
import {DataService} from '@shared/services/data.service';

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  baseUrl='item';
  constructor(private dataService: DataService) { }

 
  getAll(){
    return this.dataService.get(this.baseUrl + '/all');
  }
  add(json_data){
    return this.dataService.post(this.baseUrl + '/add', json_data);
  }
  Invoiceadd(json_data){
    return this.dataService.post(this.baseUrl + '/Invoiceadd', json_data);
  }

  
  getItembyname(data){
    return this.dataService.post(this.baseUrl +'/getItembyname', data);
  }
}
