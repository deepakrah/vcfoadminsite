import { Component, OnDestroy, OnInit } from '@angular/core';
import {ItemService} from '../item.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-items-list',
  templateUrl: './items-list.component.html',
  styleUrls: ['./items-list.component.scss']
})
export class ItemsListComponent implements OnInit,OnDestroy {

  rows: any[];
  loadingIndicator: boolean;
  reorderable: boolean;
  subscriptions: Subscription[] = [];
  dtColumnsReorderOptions: any = {};

  constructor(private itemService: ItemService) { }
  
  ngOnInit(): void {
    this.subscriptions.push(this.itemService.getAll().subscribe((d: any) => {
      this.rows = d;
    }));
     
  }
  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe())
  }


}
