import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { CategoryService } from '../category/category.service';
import { ServicesService } from '../services/services.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {
  form: FormGroup;
  isSubmit;
  updateId;
  openedModalRef;
  rows: any[];
  rows_Category: any[];
  saving = false;
  @ViewChild('servicesModal') public servicesModal;
  constructor(
    modelConfig: NgbModalConfig,
    private modalService: NgbModal,
    private _formBuilder: FormBuilder,
    private _servicesService: ServicesService,
    private _categoryService: CategoryService,
    private spinner: NgxSpinnerService,
    private toastrService: ToastrService
  ) {
    modelConfig.centered = true;
    modelConfig.backdrop = 'static';
    modelConfig.keyboard = false;
    modelConfig.size = 'md';
    this.LoadCategoryData();
    this.LoadData();
  }

  ngOnInit(): void {
    this.form = this._formBuilder.group({
      id: [0],
      name: ['', Validators.required],
      code: ['', Validators.required],
      category_id: ['', Validators.required],
      price: ['', Validators.required]
    });
  }

  get f() {
    return this.form.controls;
  }

  LoadCategoryData() {
    this._categoryService.GetAllCategories().subscribe(res => {
      this.rows_Category = res;
    });
  }

  LoadData() {
    this.spinner.show();
    this._servicesService.GetAllServices().subscribe(res => {
      this.spinner.hide();
      this.rows = res;
    });
  }

  edit(item, modalRef) {

    this.updateId = item.id;
    for (const frmCtrl in this.form.controls) {
      this.form.controls[frmCtrl].setValue(item[frmCtrl]);
    }

    this.openedModalRef = this.modalService.open(modalRef);
    console.log(this.updateId);
  }

  openModal(modalRef) {
    this.form = this._formBuilder.group({
      id: [0],
      name: ['', Validators.required],
      code: ['', Validators.required],
      category_id: ['', Validators.required],
      price: ['', Validators.required]
    });
    this.openedModalRef = this.modalService.open(modalRef);
  }

  closeModal() {
    this.openedModalRef.close();
  }


  save() {
    this.saving = true
    this.isSubmit = true;
    if (this.form.valid) {
      this.spinner.show();
      this._servicesService.SaveServices(this.form.value).subscribe(res => {
        this.spinner.hide();
        this.isSubmit = false;
        this.LoadData();
        this.closeModal();
        this.toastrService.success('service added successfully.', 'Invoice');
      });
    }
    else {
      this.saving = false;
    }
  }

}
