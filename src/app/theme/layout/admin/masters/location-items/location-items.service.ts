import { Injectable } from '@angular/core';
import {DataService} from '@shared/services/data.service';

@Injectable({
  providedIn: 'root'
})
export class LocationItemsService {
  baseUrl = 'locationitems';
  constructor(private dataService: DataService) { }


  getAll() {
    return this.dataService.get(this.baseUrl + '/all');
  }
  add(json_data){
    return this.dataService.post(this.baseUrl + '/add', json_data);
  }
}
