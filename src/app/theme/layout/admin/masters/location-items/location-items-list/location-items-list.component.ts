import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { LocationItemsService } from '../location-items.service';

@Component({
  selector: 'app-location-items-list',
  templateUrl: './location-items-list.component.html',
  styleUrls: ['./location-items-list.component.scss']
})
export class LocationItemsListComponent implements OnInit, OnDestroy {
  rows: any[];
  loadingIndicator: boolean;
  reorderable: boolean;
  subscriptions: Subscription[] = []
  constructor(private locationItemsService: LocationItemsService) { }

  ngOnInit(): void {
    this.subscriptions.push(this.locationItemsService.getAll().subscribe((d: any) => {
      this.rows = d;
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe())
  }

}
