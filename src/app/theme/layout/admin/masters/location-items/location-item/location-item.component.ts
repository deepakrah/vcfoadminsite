import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
 
import { Subscription } from 'rxjs';
import { LocationItemsService } from '../location-items.service';
import { ItemService } from '../../items/item.service';
import { LocationService } from '../../locations/location.service'

@Component({
  selector: 'app-location-item',
  templateUrl: './location-item.component.html',
  styleUrls: ['./location-item.component.scss']
})
export class LocationItemComponent implements OnInit, OnDestroy {
  form: FormGroup;
  items: any[];
  locations: any[];
  subscriptions: Subscription[] = [];
  constructor(private _formBuilder: FormBuilder, private locationItemsService: LocationItemsService,
     private itemService: ItemService, private locationService: LocationService,
     ) { }


  ngOnInit(): void {
    this.loadItems();
    this.loadLocations();
    // Reactive Form
    this.form = this._formBuilder.group({
      itemId: [, Validators.required],
      locationId: [, Validators.required],
      mrp: ['', Validators.required],
      purchasePrice: [''],
      openingStock: [''],
    });


  }
  get f(){
    return this.form.controls;
  }

  loadItems() {
    this.subscriptions.push(this.itemService.getAll().subscribe((results: any[]) => {
      this.items = results;
    },
      error => console.error(error)
    ))
  }
  loadLocations() {
    this.subscriptions.push(this.locationService.getAll().subscribe((results: any[]) => {
      this.locations = results;
    },
      error => console.error(error)
    ));
  }
isSubmit;
  add() {
    this.isSubmit = true;
    if (this.form.valid) {
    
      var d :any = {};
      Object.assign(d, this.form.value);
      d.itemId = parseInt(d.itemId);
      d.locationId = parseInt(d.locationId);
      d.mrp = parseFloat(d.mrp);
      d.purchasePrice = parseFloat(d.purchasePrice);
      d.openingStock = parseFloat(d.openingStock);

      this.subscriptions.push(this.locationItemsService.add(d).subscribe(result => {
        this.form.reset();
         
      },
        error => {
          console.error(error);
           
        }
      ));
    }
  }
  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe())
  }

}
