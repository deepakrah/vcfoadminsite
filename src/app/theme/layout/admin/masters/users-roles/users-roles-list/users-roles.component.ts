import { Component, OnDestroy, OnInit, ViewChild  } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import {NgbModal,NgbModalConfig,NgbNavChangeEvent} from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { UsersRolesService } from '../users-roles.service';
import { LocationService } from "../../locations/location.service";
@Component({
  selector: 'app-users-roles',
  templateUrl: './users-roles.component.html',
  styleUrls: ['./users-roles.component.scss'],
  providers: [NgbModalConfig, NgbModal],
})
export class UsersRolesComponent implements OnInit, OnDestroy {
  rows: any[];
  loadingIndicator: boolean;
  locations: any[];
  roles: any[];
  reorderable: boolean;
  subscriptions: Subscription[] = [];
  form: FormGroup;
  roleForm: FormGroup;
  isSubmit;
  isRoleSubmit;
  updateId;
  updateRoleId;
  saving = false;
  savingRole = false;
  openedModalRef;
  activeTab = 1;
  @ViewChild('userModal') public userModal;
  constructor(modelConfig: NgbModalConfig,
  private modalService: NgbModal,
  private _formBuilder: FormBuilder,private userService: UsersRolesService,
  private locationService: LocationService
  ) {
    modelConfig.centered = true;
    modelConfig.backdrop = 'static';
    modelConfig.keyboard = false;
    modelConfig.size = 'xl';
    console.log('Users Constructor');
   }

  ngOnInit(): void {
    
    this.form = this._formBuilder.group({
      fullName: [, Validators.required],
      email: [, Validators.required],
      mobile: [, Validators.required],
      roleId: [,],
      loginName: [, Validators.required],
      locationId: [],
      active: ["1"]
    });
    this.roleForm = this._formBuilder.group({
      name: ['', Validators.required],
      description: [],
      active: ["1"]
    });
    this.loadUsers();
    this.loadLocations();
    this.loadRoles();
  }
  loadUsers(){
    this.subscriptions.push(this.userService.getAll().subscribe((d: any) => {
      this.rows = d;
    }));
  }
  loadLocations() {
    this.subscriptions.push(this.locationService.getAll().subscribe((results: any[]) => {
      this.locations = results;
    },
      error => console.error(error)
    ));
  }

  loadRoles() {
    this.subscriptions.push(this.userService.getAllRoles().subscribe((results: any[]) => {
      this.roles = results;
    },
      error => console.error(error)
    ));
  }


  get f(){
    return this.form.controls;
  }

  get roleF(){
    return this.roleForm.controls;
  }

  save() {
    this.saving = true
    this.isSubmit = true;
    if (this.form.valid) {
      
      var d :any = {};
      Object.assign(d, this.form.value);
      d.locationId = parseInt(d.locationId);
      d.active = parseInt(d.active);
      if(d.roleId){
        d.roleId = parseInt(d.roleId);
      }
      if(this.updateId)
      {
        d['id'] = parseInt(this.updateId);
      }
      this.subscriptions.push(this.userService.save(d,this.updateId).subscribe(
      result => {
        this.isSubmit = false;
        this.form.reset();
        this.closeModal();
        this.saving = false;
        if(this.updateId){
          const updateRowIndex = this.rows.findIndex(x => x.id === this.updateId);
          this.rows[updateRowIndex] = d;
          this.rows[updateRowIndex]['roleName'] = this.roles[this.roles.findIndex(x => x.id === d.roleId)]['name'];
          this.updateId = null;
        }else{
          result['roleName'] = this.roles[this.roles.findIndex(x => x.id === d.roleId)]['name'];
          this.rows.push(result);
        }
      },
      error => {
          console.error(error);
            this.saving = false;
        }
      ));
    }
    else{
      this.saving = false;
    }
  }

  saveRole() {
    this.savingRole = true
    this.isRoleSubmit = true;
    if (this.roleForm.valid) {
      var d :any = {};
      Object.assign(d, this.roleForm.value);
      d.active = parseInt(d.active);
      if(this.updateRoleId)
      {
        d['id'] = parseInt(this.updateRoleId);
      }
      this.subscriptions.push(this.userService.saveRole(d,this.updateRoleId).subscribe(
      result => {
        this.isRoleSubmit = false;
        this.roleForm.reset();
        this.closeModal();
        this.savingRole = false;
        if(this.updateRoleId){
          const updateRowIndex = this.roles.findIndex(x => x.id === this.updateRoleId);
          for(const dItem in d){
            this.roles[updateRowIndex][dItem] = d[dItem];
          }
          this.updateRoleId = null;
        }else{
          this.roles.push(result);
        }
      },
      error => {
          console.error(error);
            this.savingRole = false;
        }
      ));
    }
    else{
      this.savingRole = false;
    }
  }
   
  edit(item,type,modalRef){
    if(type==='user'){
      this.updateId = item.id;
      for(const frmCtrl in this.form.controls){
        this.form.controls[frmCtrl].setValue(item[frmCtrl]);
      }
    }
    if(type==='role'){
      this.updateRoleId = item.id;
      for(const frmCtrl in this.roleForm.controls){
        this.roleForm.controls[frmCtrl].setValue(item[frmCtrl]);
      }
    }
    this.openedModalRef = this.modalService.open(modalRef);
    console.log(this.updateId);
  }
  openModal(modalRef){
    this.openedModalRef = this.modalService.open(modalRef);
  }
  closeModal(){
   this.openedModalRef.close();
  }
  getJsonStr(json){
    return JSON.stringify(json);
  }

  onNavChange(changeEvent: NgbNavChangeEvent) {
    if (changeEvent.nextId === 1) {
      this.loadUsers();
    }
    if (changeEvent.nextId === 2) {
      this.loadRoles();
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe())
  }

}
