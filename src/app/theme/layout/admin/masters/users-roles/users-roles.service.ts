import { Injectable } from '@angular/core';
import {DataService} from '@shared/services/data.service';

@Injectable({
  providedIn: 'root'
})
export class UsersRolesService {
  baseUrl = 'user';
  baseRoleUrl = 'role';
  constructor(private dataService: DataService) { }


  getAll() {
    return this.dataService.get(this.baseUrl + '/all');
  }
  save(json_data,id) {
    if(id){
      return this.dataService.post(this.baseUrl + '/update', json_data);
    }else{
      return this.dataService.post(this.baseUrl + '/add', json_data);
    }
    
  }
  getAllRoles(){
    return this.dataService.get(this.baseRoleUrl + '/all');
  }
  saveRole(json_data,id) {
    if(id){
      return this.dataService.post(this.baseRoleUrl + '/update', json_data);
    }else{
      return this.dataService.post(this.baseRoleUrl + '/add', json_data);
    }
    
  }
}
