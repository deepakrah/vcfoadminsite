import { Component, OnInit , forwardRef, Input} from '@angular/core';
import { ControlValueAccessor, FormControl, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';

export const  AUTOCOMPLETE2_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => Autocomplete2Component),
  multi: true
};
@Component({
  selector: 'app-autocomplete2',
  templateUrl: './autocomplete2.component.html',
  styleUrls: ['./autocomplete2.component.scss'],
  providers: [AUTOCOMPLETE2_VALUE_ACCESSOR]
})
export class Autocomplete2Component implements ControlValueAccessor {
  @Input()
  parentForm: FormGroup;
  @Input()
  fieldName: string;
   @Input()
  isSubmit : boolean = false;
  disabled: boolean;
  value: string;
  onChange: (value: string) => void;
  onTouched: () => void;
  //constructor() { }

  // ngOnInit(): void {
  // }
   get formField(): FormControl
   {
     return this.parentForm?.get(this.fieldName) as FormControl;
   }

  writeValue(obj: string): void {
    //throw new Error('Method not implemented.');
    this.value=obj ? obj :'';
  }
  onChanged(event:Event): void{
    const value: string=(<HTMLInputElement> event.target).value;
    
    this.onChange(value)
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  
  }
  
}
