import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../admin.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  rows: any[];
  constructor(
    private _adminService: AdminService,
    private spinner: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    this.spinner.show();
    this._adminService.GetAllUsers().subscribe(res => {
      this.rows = res;
      this.spinner.hide();
    });
  }

}
