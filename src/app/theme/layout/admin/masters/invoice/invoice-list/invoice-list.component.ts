import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

import { Subscription } from 'rxjs';
//import { LocationItemsService } from '../location-items.service';
// import { ItemService } from '../../items/item.service';
// import { LocationItemsService } from '../../location-items/location-items.service';
// import { LocationService } from '../../locations/location.service'

@Component({
  selector: 'app-invoice-list',
  templateUrl: './invoice-list.component.html',
  styleUrls: ['./invoice-list.component.scss']
})
export class InvoiceListComponent implements OnInit, OnDestroy {
  rows = [];
  form: FormGroup;
  items: any[];
  locations: any[];
  subscriptions: Subscription[] = [];
  constructor(
  ) { }


  ngOnInit(): void {
    this.loadItems();


  }
  get f() {
    return this.form.controls;
  }

  loadItems() {

    this.rows = [{
      invoice_no: '0001',
      name: 'Coustomer 1',
      tax_amount: 110,
      amount: 900,
      mobile: 9999857464,
      dates: '12/04/2021'
    },
    {
      invoice_no: '0002',
      name: 'Coustomer 2',
      tax_amount: 1100,
      amount: 9000,
      mobile: 7999857464,
      dates: '11/04/2021'
    }]
    // this.subscriptions.push(this.itemService.getAll().subscribe((results: any[]) => {
    //   this.items = results;
    // },
    //   error => console.error(error)
    // ))
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe())
  }

}
