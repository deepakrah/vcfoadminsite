import { Component, OnInit, OnDestroy, Injectable } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { CurrencyPipe } from '@angular/common';
import { MastersSharedService } from '../../masters-shared.service';
import { ItemService } from '../../items/item.service';
import { InvoiceService } from '../invoice.service';
import { NgbCalendar, NgbDate, NgbDateStruct, NgbDateParserFormatter, NgbInputDatepickerConfig, NgbTabsetConfig, NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
@Injectable()
export class CustomAdapter extends NgbDateAdapter<string> {

  readonly DELIMITER = '-';

  fromModel(value: string | null): NgbDateStruct | null {
    if (value) {
      let date = value.split(this.DELIMITER);
      return {
        day: parseInt(date[0], 10),
        month: parseInt(date[1], 10),
        year: parseInt(date[2], 10)
      };
    }
    return null;
  }

  toModel(date: NgbDateStruct | null): string | null {
    return date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : null;
  }
}

@Injectable()
export class CustomDateParserFormatter extends NgbDateParserFormatter {

  readonly DELIMITER = '/';

  parse(value: string): NgbDateStruct | null {
    if (value) {
      let date = value.split(this.DELIMITER);
      return {
        day: parseInt(date[0], 10),
        month: parseInt(date[1], 10),
        year: parseInt(date[2], 10)
      };
    }
    return null;
  }

  format(date: NgbDateStruct | null): string {
    return date ? (date.day < 10 ? '0' + date.day : date.day) + this.DELIMITER + (date.month < 10 ? '0' + date.month : date.month) + this.DELIMITER + date.year : '';
  }
}


@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss'],
  providers: [

    { provide: NgbDateParserFormatter, useClass: CustomDateParserFormatter }
  ]
})
export class InvoiceComponent implements OnInit {
  invoiceItem = [];
  totalTaxSum: number = 0;
  totalSum: number = 0;
  form: FormGroup;
  units: any[];
  subscriptions: Subscription[] = [];

  customerId: number;
  myFormValueChanges$;
  listCustomer = [{
    Id: 1,
    name: "Customer 1"
  },
  {
    Id: 2,
    name: "Customer 2"
  },
  {
    Id: 3,
    name: "Customer 3"

  }

  ];
  listItem = [];
  keyword = 'name';
  itemList = [];
  get customer(): FormControl {
    return this.form?.get('customer') as FormControl;
  }



  constructor(private _formBuilder: FormBuilder,
    private mastersSharedService: MastersSharedService,
    private itemService: ItemService,
    private invoiceService: InvoiceService,
    private currencyPipe: CurrencyPipe,
    private spinner: NgxSpinnerService,
    config: NgbInputDatepickerConfig, calendar: NgbCalendar, private toastrService: ToastrService) {
    // customize default values of datepickers used by this component tree
    config.minDate = { year: 1900, month: 1, day: 1 };
    config.maxDate = { year: 2099, month: 12, day: 31 };

    // days that don't belong to current month are not visible
    config.outsideDays = 'hidden';

    // weekends are disabled
    config.markDisabled = (date: NgbDate) => calendar.getWeekday(date) >= 6;

    // setting datepicker popup to close only on click outside
    //config.autoClose = 'outside';

    // setting datepicker popup to open above the input
    config.placement = ['bottom-left', 'top-right'];
  }


  ngOnInit(): void {
    // this.loadUnits();
    // Reactive Form
    this.subscriptions.push(this.itemService.getAll().subscribe((data: any) => {
      this.listItem = data;

    }));

    this.subscriptions.push(this.itemService.getItembyname({ name: '' }).subscribe((data: any) => {
      this.itemList = data;

    }));
    this.form = this._formBuilder.group({
      customer: [''],
      invioceNo: ['', Validators.required],
      invioceDate: [''],
      refBy: [''],
      // patientName:[''],
      address: ['', Validators.required],
      mobileNo: [''],
      // doctorName :[''],
      // productName:['', Validators.required],
      units: this._formBuilder.array([
        // load first row at start
        this.getUnit()
      ])
    });

    // initialize stream on units
    this.myFormValueChanges$ = this.form.controls['units'].valueChanges;
    // subscribe to the stream so listen to changes on units
    this.myFormValueChanges$.subscribe(units => this.updateTotalUnitPrice(units));



  }

  SelelectedCustomer(event: any): void {
    //alert(JSON.stringify(event));
    this.customerId = event;
  }
  get unitList() { return this.f.units as FormArray }
  get f() {
    return this.form.controls;
  }
  loadUnits() {
    this.subscriptions.push(this.mastersSharedService.getAllItemUnit().subscribe((results: any[]) => {
      this.units = results;
    },
      error => console.error(error)
    ));
  }
  isSubmit = false;
  add() {
    debugger;
    this.isSubmit = true;

    const units = this.form.value["units"];
    //  const educationRecord: number = units.length;
    //console.log(units)
    this.invoiceItem = [];
    units.forEach((item: any, index: number) => {

      const itemObj = {
        invoice_id: 0,
        item_id: item.unitName.id,
        batch_no: '',
        quantity: Number(item.qty),
        rate: Number(item.unitPrice),
        amount: 0,// Number(item.unitTotalPrice.slice(1, item.unitTotalPrice.length)),
        discount: Number(item.discount),
        discount_type: 1,
        tax_amount: Number(item.taxAmount),
        total: 0.00,
        other_charges: 0.00,
        tax_id: 25,
        tax_rate: Number(item.taxRate),
        total_amount: Number(item.unitTotalPrice.slice(1, item.unitTotalPrice.length))


      }
      this.invoiceItem.push(itemObj);
      // console.log(this.invoiceItem);
    });
    if (this.form.valid) {

      const invioce = this.form.value;

      const invioceDate = invioce.invioceDate.year + '/' + (invioce.invioceDate.month < 10 ? '0' + invioce.invioceDate.month : invioce.invioceDate.month) + '/' + (invioce.invioceDate.day < 10 ? '0' + invioce.invioceDate.day : invioce.invioceDate.day);
      //console.log(invioce)
      const invoiceObj = {
        party_id: this.customerId,
        invoice_no: invioce.invioceNo,
        party_mobile: invioce.mobileNo,
        ref_by: invioce.refBy,
        due_date: invioceDate,        //'2021/05/03',
        sub_total: 0,
        tax_amount: this.totalTaxSum,
        other_charges: 0,
        total: this.totalSum,
        // d.remarks='';
        //d.checksum='';
        //d.location_id=0,
        InvoiceDetailList: this.invoiceItem
      }

      // console.log(invoiceObj);
      this.spinner.show();
      this.subscriptions.push(this.itemService.Invoiceadd(invoiceObj).subscribe(result => {
        this.isSubmit = false;
        this.form.reset();

        this.customerId = null;
        this.form.get('invioceNo').clearValidators();
        this.form.get('invioceNo').updateValueAndValidity();
        this.form.get('invioceDate').clearValidators();
        this.form.get('invioceDate').updateValueAndValidity();
        this.form.get('address').clearValidators();
        this.form.get('address').updateValueAndValidity();
        this.clearAllUnits();
        this.spinner.hide();
        this.toastrService.success('Invoice added successfully.', 'Invoice');
        // console.log(result);

        // this.form.reset();
        // this._snackBar.open('Item added', 'Close', {
        //   duration: 2000,
        // });
      },
        error => {
          console.log(error);
          // this._snackBar.open('Failure in Adding Item', 'Close', {
          //   duration: 2000,
          // });
        }
      ));
    }
  }
  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe())
  }

  /**
  * Create form unit
  */
  private getUnit() {
    const numberPatern = '^[0-9.,]+$';
    return this._formBuilder.group({
      unitName: ['', Validators.required],
      qty: [1, [Validators.required, Validators.pattern(numberPatern)]],
      unitPrice: ['', [Validators.required, Validators.pattern(numberPatern)]],
      discount: [0],
      taxRate: [0],
      taxAmount: [],
      unitTotalPrice: []
    });
  }

  /**
   * Add new unit row into form
   */
  addUnit() {
    const control = <FormArray>this.form.controls['units'];
    control.push(this.getUnit());
    this.isSubmit = false;
  }

  /**
   * Remove unit row from form on click delete button
   */
  removeUnit(i: number) {
    const control = <FormArray>this.form.controls['units'];
    control.removeAt(i);
  }

  /**
   * This is one of the way how clear units fields.
   */
  clearAllUnits() {
    const control = <FormArray>this.form.controls['units'];
    while (control.length) {
      control.removeAt(control.length - 1);
    }
    control.clearValidators();
    control.push(this.getUnit());
  }

  /**
* Update prices as soon as something changed on units group
*/
  private updateTotalUnitPrice(units: any) {
    // get our units group controll
    const control = <FormArray>this.form.controls['units'];
    // before recount total price need to be reset. 

    this.totalSum = 0;
    this.totalTaxSum = 0;
    for (let i in units) {


      let discount = units[i].discount;
      let price = (units[i].qty * units[i].unitPrice);
      let discountAmount = (price * discount) / 100;
      let taxableAmount = price - discountAmount;
      let taxAmout = (taxableAmount * units[i].taxRate) / 100;

      // let totalUnitPrice = (units[i].qty * units[i].unitPrice);
      let totalUnitPrice = price - discountAmount + taxAmout;
      // now format total price with angular currency pipe
      let totalUnitPriceFormatted = this.currencyPipe.transform(totalUnitPrice, 'INR', 'symbol-narrow', '1.2-2');
      // update total sum field on unit and do not emit event myFormValueChanges$ in this case on units
      control.at(+i).get('taxAmount').setValue(taxAmout, { onlySelf: true, emitEvent: false });
      control.at(+i).get('unitTotalPrice').setValue(totalUnitPriceFormatted, { onlySelf: true, emitEvent: false });
      // update total price for all units
      this.totalTaxSum += taxAmout;
      this.totalSum += totalUnitPrice;
    }
  }



  selectItemEvent(item: any, index: number) {
    // do something with selected item
    // alert(JSON.stringify(item));
    let taxamout = 0;

    (<FormArray>this.form.controls['units']).at(index).patchValue({
      unitName: item.name,
      qty: 1,
      unitPrice: item.rate,
      discount: item.discount,
      taxRate: item.tax_rate,
      taxAmount: taxamout

    });
    //   
    this.form.updateValueAndValidity()
    // this.addUnit()
    // console.log(this.form.value);
  }

  onItemChangeSearch(name: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
    //if (name && name.length >= 0) {
    // this.itemService.getItembyname({ name: name })
    //  this.itemService.getAll()
    //    .subscribe((res: any) => {
    //       if (res == undefined) {
    //         this.itemList = [];
    //       }
    //       if (res && res.length > 0) {

    //         this.itemList = res.map((opt: any) => {
    //           return opt.name;
    //         });
    //       }

    //     },
    //       err => {
    //         //  alert(JSON.stringify(err));
    //       });
    //}
  }

  onItemFocused(e) {
    // do something when input is focused
  }


}
