import { Injectable } from '@angular/core';
import {DataService} from '@shared/services/data.service';

@Injectable({
  providedIn: 'root'
})
export class InvoiceService {

  baseUrl='http://localhost:44337/sale';
  constructor(private dataService: DataService) {

   }
   
  add(json_data: any){
    return this.dataService.postNew(this.baseUrl + '/add', json_data);
  } 
}
