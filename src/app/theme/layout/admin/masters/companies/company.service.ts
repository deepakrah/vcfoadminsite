import { Injectable } from '@angular/core';
import {DataService} from '@shared/services/data.service';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {
  baseUrl = 'company';
  constructor(private companyService: DataService) { }


  getAll() {
    return this.companyService.get(this.baseUrl + '/all');
  }
  add(json_data){
    return this.companyService.post(this.baseUrl + '/add', json_data);
  }
}
