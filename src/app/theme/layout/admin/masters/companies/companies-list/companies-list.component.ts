import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CompanyService } from '../company.service';

@Component({
  selector: 'app-companies-list',
  templateUrl: './companies-list.component.html',
  styleUrls: ['./companies-list.component.scss']
})
export class CompaniesListComponent implements OnInit, OnDestroy {
  rows: any[];
  loadingIndicator: boolean;
  reorderable: boolean;
  subscriptions: Subscription[] = []
  constructor(private companyService: CompanyService) { }

  ngOnInit(): void {
    this.subscriptions.push(this.companyService.getAll().subscribe((d: any) => {
      this.rows = d;
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe())
  }

}
