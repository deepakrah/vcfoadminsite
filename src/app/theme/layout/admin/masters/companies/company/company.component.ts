import { Component, OnInit,OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
 
import { CompanyService } from '../company.service';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit, OnDestroy {
  form: FormGroup;
  subscriptions: Subscription[] = [];
  constructor(private _formBuilder: FormBuilder,private companyService: CompanyService) { }

  
  ngOnInit(): void
  {
      // Reactive Form
      this.form = this._formBuilder.group({
          name : [''],
          dba : [''],
          address1  : [''],
          address2   : [''],
          phone   : [''],
          email   : [''],
          businessCategoryId   : [],
          typeId   : []
      });

      
  }
  add(){
    const json_str = JSON.stringify(this.form.value);
    this.subscriptions.push(this.companyService.add(json_str).subscribe(result => {
     
    }, 
    error =>{ console.error(error);
     
  }
    ));
  }
  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe())
  }
}
