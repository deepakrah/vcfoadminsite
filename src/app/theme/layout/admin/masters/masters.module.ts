import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { ItemsListComponent } from './items/items-list/items-list.component';
import { ItemComponent } from './items/item/item.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CompaniesListComponent } from './companies/companies-list/companies-list.component';
import { CompanyComponent } from './companies/company/company.component';
import { LocationsListComponent } from './locations/locations-list/locations-list.component';
import { LocationComponent } from './locations/location/location.component';
import { ItemsUnitListComponent } from './items-unit/items-unit-list/items-unit-list.component';
import { ItemUnitComponent } from './items-unit/item-unit/item-unit.component';
import { LocationItemComponent } from './location-items/location-item/location-item.component';
import { HSNComponent } from './hsns/hsn/hsn.component';
import { HSNListComponent } from './hsns/hsn-list/hsn-list.component';
import { UsersRolesComponent } from './users-roles/users-roles-list/users-roles.component';
import { InvoiceComponent } from './invoice/invoice/invoice.component';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { AutocompleteComponent } from './autocomplete/autocomplete.component';
import { Autocomplete2Component } from './autocomplete2/autocomplete2.component';
import { Autocomplete3Component } from './autocomplete3/autocomplete3.component';
import { InvoiceListComponent } from './invoice/invoice-list/invoice-list.component';
import { CategoryComponent } from './category/category.component';
import { ServicesComponent } from './services/services.component';
import { UsersComponent } from './users/users.component';
import { OrdersComponent } from './orders/orders.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
const routes: Routes = [
  {
    path: '',
    redirectTo: 'items'
  },
  {
    path: 'category',
    component: CategoryComponent
  },
  {
    path: 'services',
    component: ServicesComponent
  },
  {
    path: 'User',
    component: UsersComponent
  },
  {
    path: 'Orders',
    component: OrdersComponent
  },
  {
    path: 'ContactUs',
    component: ContactUsComponent
  },
  {
    path: 'items',
    component: ItemsListComponent
  },
  {
    path: 'items/add',
    component: ItemComponent
  },
  {
    path: 'ogrs',
    component: CompaniesListComponent
  },
  {
    path: 'orgs/add',
    component: CompanyComponent
  },
  {
    path: 'locations',
    component: LocationsListComponent
  },
  {
    path: 'locations/add',
    component: LocationComponent
  },
  {
    path: 'locations/update/:id',
    component: LocationComponent
  },
  {
    path: 'units',
    component: ItemsUnitListComponent
  },
  {
    path: 'units/add',
    component: ItemUnitComponent
  },
  {
    path: 'units/update/:id',
    component: ItemUnitComponent
  },
  {
    path: 'locItems',
    component: LocationItemComponent
  },
  {
    path: 'hsn',
    component: HSNListComponent
  },
  {
    path: 'hsn/add',
    component: HSNComponent
  },
  {
    path: 'hsn/update/:id',
    component: HSNComponent
  },
  {
    path: 'usersroles',
    component: UsersRolesComponent
  },
  {
    path: 'invoice/add',
    component: InvoiceComponent
  },
  {
    path: 'invoice',
    component: InvoiceListComponent
  }
]


@NgModule({
  declarations: [ItemComponent, ItemsListComponent, CompaniesListComponent, CompanyComponent,
    LocationsListComponent, LocationComponent,
    ItemUnitComponent, ItemsUnitListComponent,
    LocationItemComponent, HSNComponent, HSNListComponent, UsersRolesComponent, InvoiceComponent, AutocompleteComponent,
    Autocomplete2Component, Autocomplete3Component, InvoiceListComponent, CategoryComponent, ServicesComponent, UsersComponent, OrdersComponent, ContactUsComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    RouterModule.forChild(routes),
    AutocompleteLibModule],
  exports: [RouterModule],
  schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA]

})
export class MastersModule { }
