import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ItemUnitService } from '../item-unit.service';


@Component({
  selector: 'app-items-unit-list',
  templateUrl: './items-unit-list.component.html',
  styleUrls: ['./items-unit-list.component.scss']
})
export class ItemsUnitListComponent implements OnInit, OnDestroy {
  rows: any[];
  loadingIndicator: boolean;
  reorderable: boolean;
  subscriptions: Subscription[] = [];
  constructor(private itemunitService: ItemUnitService) { }

  ngOnInit(): void {
    this.itemunitService.getAll().subscribe((d: any) => {

      this.rows = d.map((item) => { item.active = item.active === 1 ? 'Yes' : 'No'; return item; });;
    });
  }
  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }
  
  getJsonStr(json){
    return JSON.stringify(json);
  }
}
