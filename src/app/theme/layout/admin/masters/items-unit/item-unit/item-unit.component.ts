import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MastersSharedService } from "../../masters-shared.service";
import { ItemUnitService } from '../item-unit.service';

@Component({
  selector: 'app-item-unit',
  templateUrl: './item-unit.component.html',
  styleUrls: ['./item-unit.component.scss']
})
export class ItemUnitComponent implements OnInit, OnDestroy {
  form: FormGroup;
  subscriptions: Subscription[] = [];
  isSubmit =false;
  updateId;
  updateObjectValues;
  constructor(private _formBuilder: FormBuilder, 
    private router: Router, private route: ActivatedRoute,
    private mastersSharedService: MastersSharedService,
    private itemUnitService: ItemUnitService) { 

      this.subscriptions.push(this.route.params.subscribe( params => {
        this.updateId = params.id;
      }));
      if(this.router.getCurrentNavigation() && this.router.getCurrentNavigation().extras && this.router.getCurrentNavigation().extras.state){
        this.updateObjectValues = this.router.getCurrentNavigation().extras.state;
      }

    }

  ngOnInit(): void {
    // Reactive Form
    this.form = this._formBuilder.group({
      code: ['', Validators.required],
      name: ['', Validators.required],
      active: []
    });
    this.patchEditFormGroupValues();
  }
  get f() {
    return this.form.controls;
  }

  patchEditFormGroupValues(){
    if(this.updateObjectValues){
      const objectValueJSON = JSON.parse(this.updateObjectValues);
      for(const frmCtrl in this.form.controls){
        if(frmCtrl !== 'active'){
          this.form.controls[frmCtrl].setValue(objectValueJSON[frmCtrl]);
        }
        else{
          this.form.controls[frmCtrl].setValue(objectValueJSON[frmCtrl] == "Yes" ? true: false);
        }
      }
    }
  }

  save() {
    this.isSubmit = true;
    if (this.form.valid) {
      if(this.updateId)
      {
        this.form.value['id'] = parseInt(this.updateId);
      }
      this.form.value['active'] = this.form.value['active'] ? 1 : 0;
      const json_str = JSON.stringify(this.form.value);

      this.subscriptions.push(this.itemUnitService.save(json_str, this.updateId).subscribe(result => {
        this.form.reset();
        if(this.updateId)
        {
          this.router.navigate(["/admin/masters/units"]);
        }

      },
        error => {
          console.error(error);

        }
      ));
    }
  }
  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe())
  }
}
