import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth.service';
import { StorageService } from '@shared/services/storage.service';

@Component({
  selector: 'app-auth-signup-v2',
  templateUrl: './auth-signup-v2.component.html',
  styleUrls: ['./auth-signup-v2.component.scss']
})
export class AuthSignupV2Component implements OnInit, OnDestroy {
  form: FormGroup;
  subscriptions: Subscription[] = [];

  constructor(private fb: FormBuilder, private router: Router,
    private auth: AuthService,
    private storage: StorageService) {
    this.form = this.fb.group({
      name: ['', Validators.required],
      typeId: [''],
      contactName: ['', Validators.required],
      phone: ['', Validators.required],
      email: ['', Validators.required]
    });

  }

  get f() {
    return this.form.controls;
  }

  ngOnInit() {
  }
  isSubmit;
  signUp() {
    
    debugger
    this.isSubmit = true;
    if (this.form.invalid) {
      return;
    }
    var model: any = {};
    Object.assign(model,this.form.value);
    model.typeId = parseInt(model.typeId);
    this.subscriptions.push(this.auth.add(model).subscribe((d: any) => {
      if (d) {
        if (d.id == 0) {
          this.storage.set('email', model.email);
          this.router.navigate(['auth/signup/verify']);
        }
      }
    }, (err) => {

    }));
  }
  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe())
  }
}
