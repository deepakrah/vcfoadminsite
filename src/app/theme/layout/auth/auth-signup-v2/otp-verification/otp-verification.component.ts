import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StorageService } from '@shared/services/storage.service';
import { OtpService } from '@shared/services/otp.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-otp-verification',
  templateUrl: './otp-verification.component.html',
  styleUrls: ['./otp-verification.component.scss']
})
export class OtpVerificationComponent implements OnInit {
  form: FormGroup;
  constructor(private fb: FormBuilder, private storage: StorageService,
    private router: Router,
    private auth: OtpService) {
    this.form = fb.group({
      otp: ['', Validators.required]
    });
  }
  get f() {
    return this.form.controls;
  }
  ngOnInit(): void {
  }
  isSubmit;
  verify() {
    debugger
    var email = this.storage.get('email');
    var otp = this.form.value.otp;
    var model = { sentTo: email, otp: otp };
    this.auth.verify(model).subscribe((d: any) => {
      this.router.navigate(['../admin/masters']);
    }, (err) => {

    });
  }
}
