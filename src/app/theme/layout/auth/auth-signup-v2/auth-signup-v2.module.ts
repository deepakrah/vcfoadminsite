import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthSignupV2RoutingModule } from './auth-signup-v2-routing.module';
import { AuthSignupV2Component } from './auth-signup-v2.component';
import { OtpVerificationComponent } from './otp-verification/otp-verification.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@shared/shared.module';

@NgModule({
  declarations: [AuthSignupV2Component, OtpVerificationComponent],
  imports: [
    CommonModule,
    AuthSignupV2RoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class AuthSignupV2Module { }
