import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignUpComponent } from './sign-up/sign-up.component';
import { AuthComponent } from './auth.component';
 

const routes: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: '',
        redirectTo: 'signup'
      },
      {
        path: 'signup',
        loadChildren: () => import('./auth-signup-v2/auth-signup-v2.module').then(module => module.AuthSignupV2Module)
      },
      {
        path: 'signin',
        loadChildren: () => import('./auth-signin/auth-signin.module').then(module => module.AuthSigninModule)
      }
    ]
  }
 
]


@NgModule({
  declarations: [SignUpComponent],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthModule { }
