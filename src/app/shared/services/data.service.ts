import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders  } from '@angular/common/http';
import {environment} from 'environments/environment'
@Injectable({
  providedIn: 'root'
})
export class DataService {


  constructor(private client: HttpClient ) { }

  get(url){
    return this.client.get(environment.apiUrl + '/' + url); 
  }

  post(url,data){
    const headers = new HttpHeaders({
        'Content-Type': 'application/json'
    });
    return this.client.post(environment.apiUrl + '/' + url, data, {headers: headers}); 
  }
  postNew(url,data){
    const headers = new HttpHeaders({
        'Content-Type': 'application/json'
    });
    return this.client.post(url, data, {headers: headers}); 
  }
}
