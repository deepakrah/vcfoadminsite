import { Injectable } from '@angular/core';
import { DataService } from './data.service';
@Injectable({
  providedIn: 'root'
})
export class OtpService {
  baseUrl = "otp";
  constructor(private dataService: DataService) { }

  verify(model) {
    return this.dataService.post(this.baseUrl + '/verify', model);
  }
  sendOtp(model){
    return this.dataService.post(this.baseUrl + '/send', model);
  }
}
